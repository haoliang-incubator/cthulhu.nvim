local M = {}

local ffi = require("ffi")
local C = require("cthulhu.c")

local api = vim.api
local uv = vim.loop

M.notify = (function()
  local nvim_icon = (function()
    local runtime = os.getenv("VIMRUNTIME")
    if runtime == nil then return end
    if not vim.endswith(runtime, "/share/nvim/runtime") then return end
    local icon = string.format("%s/%s", string.sub(runtime, 1, #runtime - #"/nvim/runtime"), "icons/hicolor/128x128/apps/nvim.png")
    local stat, errmsg, err = uv.fs_stat(icon)
    if stat ~= nil then return icon end
    if err == "ENOENT" then return end
    api.nvim_err_writeln(errmsg)
  end)() or ""

  local function notify(urgency)
    assert(urgency)

    ---@param summary string
    ---@param body string|nil
    ---@param timeout number|nil @1000ms
    ---@param icon string|nil
    return function(summary, body, icon, timeout)
      assert(summary ~= nil)
      body = body or ""
      icon = icon or nvim_icon
      timeout = timeout or 1000

      ---@diagnostic disable: undefined-field
      return C.cthulhu_notify(summary, body, icon, urgency, timeout) == 1
    end
  end

  return setmetatable({
    low = notify(0),
    normal = notify(1),
    critical = notify(2),
  }, {
    __call = function(cls, ...)
      return cls.normal(...)
    end,
  })
end)()

function M.md5(str)
  assert(str ~= nil)
  local len = 32
  local hex = ffi.new("char *[?]", len)
  C.cthulhu_md5hex(str, hex)
  return ffi.string(hex, len)
end

return M
