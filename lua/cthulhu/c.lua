local ffi = require("ffi")

--points to cthulhu.nvim/
local root = (function()
  -- thanks to bfredl for this solution: https://github.com/neovim/neovim/issues/20340#issuecomment-1257142131
  local source = debug.getinfo(1, "S").source
  assert(vim.startswith(source, "@") and vim.endswith(source, "c.lua"), "failed to resolve the root dir of cthulhu.nvim")
  return vim.fn.fnamemodify(string.sub(source, 2), ":h:h:h")
end)()

local function so(name)
  if name ~= "cthulhu" then name = "cthulhu-" .. name end
  return string.format("%s/%s/lib%s.so", root, "zig-out/lib", name)
end

ffi.cdef([[
  int cthulhu_notify(const char *summary, const char *body, const char *icon, unsigned int urgency, int timeout);
  void cthulhu_md5hex(const char *str, char *digest[32]);
]])

_ = ffi.load(so("notify"), true)
_ = ffi.load(so("md5"), true)

return ffi.C
